import gzip
from nplm import nplm
import pickle
from render import render
import scipy.io as scio
from tsne import tsne

"""
Neural Probabilistic Language Model.
"""

def main():
    """
    Load a dataset of word-index sequences, a dictionary mapping these indices
    to their respective words and train a neural probabilistic language on
    these word sequences. The evaluation measure is cross entropy.
    """

    # Load word data.
    data = pickle.load(gzip.open('data.pkl.gz', 'rb'))
    train_input = data['train_input']
    train_label = data['train_label']
    test_input = data['test_input']
    test_label = data['test_label']
    valid_input = data['valid_input']
    valid_label = data['valid_label']
    vocab = data['vocab']

    n_words = train_input.shape[1]
    vocab_size = len(vocab)

    # Initialize model.
    l_model = nplm(vocab_size=vocab_size, n_words=n_words, n_embeddings=50,
                   n_hidden=100, learning_rate=0.1, weight_decay=0.0001,
                   initial_momentum=0.5, final_momentum=0.9, batch_size=100,
                   n_iter=10, verbose=True, random_state=42)
    l_model.fit(train_input, train_label)

    pickle.dump(l_model, gzip.open('language-model.pkl.gz', 'wb'))
    
    ce_train = l_model.score(train_input, train_label)
    ce_valid = l_model.score(valid_input, valid_label)
    ce_test = l_model.score(test_input, test_label)

    print("Training cross entropy: %.3f" % ce_train)
    print("Validation cross entropy: %.3f" % ce_valid)
    print("Test cross entropy: %.3f\n" % ce_test)

    # Use t-SNE to map the features in X_real to 2D. 
    print("Generating 2D map of words using t-SNE...")
    Z = tsne(l_model.w_in_emb_, no_dims=2, perplexity=25.0)
    labels = []
    for idx in vocab.keys():
        labels.insert(idx, vocab[idx])

    # Render the t-SNE generated low-dimensional features.
    render([(label, pt[0], pt[1]) for label, pt in zip(labels, Z)], 
           "word-map.png", width=1024, height=768) 
 

if __name__ == '__main__':
    main()
