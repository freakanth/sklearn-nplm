import numpy as np

from sklearn.externals.six.moves import xrange # I don't know why
from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin
from sklearn.utils import check_random_state
from sklearn.utils import check_arrays
from sklearn.utils import gen_even_slices
from sklearn.utils.extmath import logistic_sigmoid
np.set_printoptions(threshold=np.nan, suppress=True, precision=3)

"""
Neural Probabilistic Language Model.
"""

# Author: Srikanth Cherla <abfb145@city.ac.uk>

def softmax(x):
    """
    Compute the softmax of a matrix where each column is considered to
    be a probability distribution.

    Input:
    ------
    N: A nxm matrix where n is the number of categories and
       m is the number of instances.
                                 
    Output:
    -------
    Softmax function applied to the matrix N.
    """
    
    x = np.exp(x - np.amax(x, axis=0))
    return x / x.sum(axis=0)


def cross_entropy(p, t):
    """
    Compute cross-entropy of the probability distribution predicted by a model,
    given the target values for certain input.

    Input:
    ------
    p: The predicted probability distributions for each sample of input data.
    t: Target values corresponding to each input.

    Output:
    -------
    ce: Cross entropy.
    """
    return -np.mean(np.log(p)[t, np.arange(t.shape[0])])


class nplm(BaseEstimator, TransformerMixin):
    """
    Class definition for a neural-probabilistic language model (NPLM) as
    described in [1] and implemented in [2].

    [1] Bengio, Yoshua et al. "Neural Probabilistic Language Models."
        Innovations in Machine Learning. Springer Berlin Heidelberg, 2006.
        137--186.

    [2] Hinton, Geoffrey. "Neural Networks for Machine Learning.", Coursera
    (Fall, 2012). http://coursera.org/course/neuralnets
    """

    def __init__(self, vocab_size=250, n_words=3, n_embeddings=50,
                 n_hidden=200, learning_rate=0.05, weight_decay=0.0001,
                 initial_momentum=0.5, final_momentum=0.9, batch_size=10,
                 n_iter=10, verbose=False, random_state=None):
        """
        Initialization function for neural language model.

        Input:
        ------
        vocab_size: Number of different words in the vocabulary.
        n_words: Length of context.
        n_embeddings: Size of the distributed representation for words.
        n_hidden: Number of hidden units.
        learning_rate: Learning rate for back-propagation.
        batch_size: Batch size for mini-batch gradient descent.
        n_iter: Number of training epochs.
        verbose: Verbose execution (True/False).
        random_state: Seed for initializing random state of weights.

        Output:
        -------
        A trained neural language model.
        """

        self.vocab_size = vocab_size
        self.n_words = n_words
        self.n_embeddings = n_embeddings
        self.n_hidden = n_hidden
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.initial_momentum = initial_momentum
        self.final_momentum = final_momentum
        self.batch_size = batch_size
        self.n_iter = n_iter
        self.verbose = verbose
        self.random_state = random_state


    def _fprop(self, X_batch):
        """
        Forward propagate the input values along the network.

        Input:
        ------
        X_batch: A batch of the training inputs.

        Output:
        -------
        s_emb: States of the embedding layer units.
        s_hid: States of the hidden layer.
        s_out: States of the output layer.
        (Go to end of script for details on the math.)
        """

        n_cases, n_words = X_batch.shape

        # Look up the states of the embedding units.
        s_emb = np.reshape(
            self.w_in_emb_[np.reshape(X_batch, (n_words*n_cases,)), :],
            (n_cases, n_words*self.n_embeddings)).T

        # Compute states of hidden units.
        i_hid = np.dot(self.w_emb_hid_.T, s_emb) + \
                np.tile(self.b_hid_[:, np.newaxis], (1, n_cases))
        s_hid = logistic_sigmoid(i_hid)

        # Compute states of output units.
        i_out = np.dot(self.w_hid_out_.T, s_hid) + \
                np.tile(self.b_out_[:, np.newaxis], (1, n_cases))
        s_out = softmax(i_out)

        return s_emb, s_hid, s_out
    
    
    def _bprop(self, X_batch, y_batch, s_emb, s_hid, s_out):
        """
        Backpropagate error values along the network.

        Input:
        ------
        y_batch: A batch of the training targets.
        s_emb: States of the embedding layer units.
        s_hid: States of the hidden layer.
        s_out: States of the output layer.
    
        Output:
        -------
        Each of the returned variables is the derivative of the error w.r.t
        grad_w_in_emb: Input-to-embedding layer weights.
        grad_w_emb_hid: Embedding-to-hidden weights.
        grad_w_hid_out: Hidden-to-output layer weights
        grad_b_hid: Hidden layer biases.
        grad_b_out: Output layer biases.
        (Go to end of script for details on the math.)

        This function call takes an extra 1.5e-04 seconds more than if its
        contents were executed directly in fit(). _bprop() gets called
        n_batches x n_iter times. For example, training over 10 epochs with
        1000 batches would take an extra 1.5 seconds. So I'm leaving it this
        way for now as it makes fit() more concise and modular.
        """
        
        exp_mat = np.eye(self.vocab_size)

        # Error derivative (dC/dz_k)
        exp_y_batch = exp_mat[:, y_batch]
        der_err = s_out - exp_y_batch

        # Output layer
        grad_w_hid_out = np.dot(s_hid, der_err.T)
        grad_b_out = der_err.sum(axis=1)
        der_bprop_1 = np.dot(self.w_hid_out_, der_err) * s_hid * (1 - s_hid)

        # Hidden layer
        grad_w_emb_hid = np.dot(s_emb, der_bprop_1.T)
        grad_b_hid = der_bprop_1.sum(axis=1)
        der_bprop_2 = np.dot(self.w_emb_hid_, der_bprop_1)

        # Embedding layer
        grad_w_in_emb = 0
        for w in xrange(self.n_words):
            grad_w_in_emb = grad_w_in_emb + \
            np.dot(exp_mat[:, X_batch[:, w]],
                   der_bprop_2[w * self.n_embeddings:(w+1) *
                               self.n_embeddings, :].T)

        return grad_w_in_emb, grad_w_emb_hid, grad_w_hid_out, grad_b_hid, \
               grad_b_out


    def score(self, X, t):
        """
        Compute the score of the trained model on input data.

        Input:
        ------
        X: Input data matrix.
        t: Target values corresponding to input.

        Output:
        -------
        score: Score (in this case, cross entropy)
        """

        _, _, s_out = self._fprop(X)
        
        return cross_entropy(s_out, t)


    def fit(self, X, y):
        """
        Fit the model to the input data X (and output data y).

        Input:
        ------
        X: A 2D Numpy array of size m x n where m is the number of data
           instances and n is the number of features per instance.
        y: A 1D Numpy array of size m, which contains the class-labels
           (targets) for each of the m input data instances.

        Output:
        -------
        self
        """
        
        X, = check_arrays(X, dtype=np.int)
        y, = check_arrays(y, dtype=np.int)
        n_samples = X.shape[0]
        rng = check_random_state(self.random_state)
        
        assert(self.n_words == X.shape[1]), \
            "Unexpected context length in training data!"
        
        # Word-to-embedding weight matrix.
        self.w_in_emb_ = np.asarray( 
            rng.normal(0, 0.01, (self.vocab_size, self.n_embeddings)),
            order='fortran')

        # Embedding-to-hidden weight matrix.   
        self.w_emb_hid_ = np.asarray( 
            rng.normal(0, 0.01, (self.n_words*self.n_embeddings,
                                 self.n_hidden)),
            order='fortran')

        # Hidden-to-output weight matrix.
        self.w_hid_out_ = np.asarray( 
            rng.normal(0, 0.01, (self.n_hidden, self.vocab_size)),
            order='fortran')

        # Hidden and output biases.
        self.b_hid_ = np.zeros(self.n_hidden,)
        self.b_out_ = np.zeros(self.vocab_size,)

        # Variables to update weights and biases (needed for use with momentum)
        delta_w_in_emb = np.zeros((self.vocab_size, self.n_embeddings),)
        delta_w_emb_hid = np.zeros((self.n_words*self.n_embeddings,
                                    self.n_hidden),)
        delta_w_hid_out = np.zeros((self.n_hidden, self.vocab_size),)
        delta_b_hid = np.zeros((self.n_hidden,),)
        delta_b_out = np.zeros((self.vocab_size,),)
        
        n_batches = int(np.ceil(float(n_samples) / self.batch_size))
        batch_slices = list(gen_even_slices(n_batches * self.batch_size,
                                            n_batches))

        verbose = self.verbose
        
        for iteration in xrange(self.n_iter):
            for batch_slice in batch_slices:
                # 1. Select data belonging to current batch.
                X_batch = X[batch_slice]
                y_batch = y[batch_slice]
                
                # 2. Forward-propagate.
                s_emb, s_hid, s_out = self._fprop(X_batch)
                
                # 3. Backpropagate.
                grad_w_in_emb, grad_w_emb_hid, grad_w_hid_out, grad_b_hid, \
                    grad_b_out = self._bprop(X_batch, y_batch, s_emb, s_hid,
                                             s_out)
                
                if iteration < 5:
                    momentum = self.initial_momentum
                else:
                    momentum = self.final_momentum
                
                # 4. Update weights and biases.
                delta_w_in_emb = momentum * delta_w_in_emb - \
                        self.learning_rate * \
                        (grad_w_in_emb / self.batch_size + \
                        self.weight_decay * self.w_in_emb_)
                self.w_in_emb_ += delta_w_in_emb
                
                delta_w_emb_hid = momentum * delta_w_emb_hid - \
                        self.learning_rate * \
                        (grad_w_emb_hid / self.batch_size + \
                        self.weight_decay * self.w_emb_hid_)
                self.w_emb_hid_ += delta_w_emb_hid
                
                delta_w_hid_out = momentum * delta_w_hid_out - \
                        self.learning_rate * \
                        (grad_w_hid_out / self.batch_size + \
                        self.weight_decay * self.w_hid_out_)
                self.w_hid_out_ += delta_w_hid_out
                
                delta_b_hid = momentum * delta_b_hid - \
                        self.learning_rate * \
                        (grad_b_hid / self.batch_size + \
                        self.weight_decay * self.b_hid_)
                self.b_hid_ += delta_b_hid
                
                delta_b_out = momentum * delta_b_out - \
                        self.learning_rate * \
                        (grad_b_out / self.batch_size + \
                        self.weight_decay * self.b_out_)
                self.b_out_ += delta_b_out

            if verbose:
                ce = self.score(X, y)
                print("Epoch %d out of %d. Training set cross entropy: %.3f" \
                      % (iteration + 1, self.n_iter, ce))
            
        return self


# Theory:
# -------
#
# The following is the structure for a neural probabilistic language model
# that uses a context length of 3 words for prediction.
#
#                       ------------------
#                       |     Word 4     |                          Index: k
#                       ------------------
#                               ^
#                               |
#       -------------------------------------------------
#       |                    Hidden                     |           Index: j
#       -------------------------------------------------
#                               ^                   
#                               |                   
#     -----------------------------------------------------  
#     | ---------           ---------           --------- |
#     | | Emb 1 |           | Emb 2 |           | Emb 3 | |         Index: i
#     | ---------           ---------           --------- |
#     -----------------------------------------------------
#           ^                   ^                   ^
#           |                   |                   |
#   ------------------  ------------------  ------------------
#   |     Word 1     |  |     Word 2     |  |     Word 3     |      Index: h
#   ------------------  ------------------  ------------------
#
#   In this model, the word (input) layer and the embedding layer are
# connected via a look-up table that converts word indices into real-valued
# feature vectors. The embedding and hidden layers are connected by a weight
# matrix with elements w_ij. The hidden layer contains logistic sigmoid units.
# The layers j and k are connected by a weight matrix with elements w_jk. The
# output layer has softmax units. Each of the hidden and output layers are also
# connected to an additional node whose value is always 1 (not depicted in the
# figure), by their respective bias vectors b_j and b_k.
#
#   The total input to any unit i in the embedding layer is given by z_i.
# The output of each unit i in the same layer is given by y_i. The same
# convention applies to the hidden and output layers with their respective
# indices j and k. The only difference is that in the hidden layer, the
# logistic sigmoid function is applied to the total input to each node j,
# while the function applied to total input to each node in the output layer
# is softmax. The target value corresponding to unit k of the output layer is
# given by t_k.
#
#   The error function to compare the network's output and the target values is
# cross entropy. This function is chosen as the slope of its derivative exactly
# compensates for that of the softmax function, making the overall gradient at
# the output layer linear. I'm not fully clear about how to explain this right
# now but I hope you get the point.
#
#   First, we cover the steps for learning the parameters between the embedding
# and output layers. The word to embedding layer parameters will be discussed
# separately after this.
#   
#   The Forward-propagation along the network is done using the following
# equations
#
#       z_j = b_j + sum_i(y_i * w_ij)
#       y_j = log_sig(z_j)
#       z_k = b_k + sum_j(y_j * w_jk)
#       y_k = softmax(z_k)
#
#   And the error is computed with the following equation
#
#       C = -sum_k(t_k * log(y_k))
#
#   For training the network, we need the following derivatives (partial
# derivatives where applicable)
#
#       (1) dC/dw_jk (2) dC/b_k (3) dC/dw_ij (4) dC/db_j
#
#   (1) dC/dw_jk can be written as
#
#           dC/dw_jk = dC/dz_k * dz_k/dw_jk
#
#       and,
#           
#           dC/dz_k = (y_k - t_k)                                      (1a)
#           dz_k/dw_jk = y_j
#
#       so, dC/dw_jk = y_j * (y_k - t_k)                                (1)
#           grad_w_hid_out = np.dot(s_hid, np.transpose(der_err))
#
#   (2) dC/db_k can be written as 
#
#           dC/db_k = dC/dz_k * dz_k/db_k
#
#       We know the value of dC/dz_k from (1a), and
#           
#           dz_k/db_k = 1
#
#       so, 
#           dC/db_k = (y_k - t_k)                                       (2)
#           grad_b_out = der_err.sum(axis=1)
#
#   (3) dC/dw_ij can be written as
#
#           dC/dw_ij = dC/dz_j * dz_j/dw_ij
#
#       where dC/dz_j is known as the backpropagated derivative w.r.t the
#       hidden layer (der_bprop_1). We can write dC/dz_j as
#
#           dC/dz_j = dC/dy_j * dy_j/dz_j 
#                   = sum_k(w_jk * (y_k-t_k)) .* y_j .* (1-y_j)        (3a)
#                     np.dot(self.w_hid_out_, der_err) * s_hid * (1 - s_hid)
#
#       where,
#           dC/dy_j = dC/dz_k * dz_k/dy_j = sum_k(w_jk * (y_k - t_k))
#           dy_j/dz_j = y_j * (1 - y_j)
#
#       and,
#           dz_j/dw_ij = y_i
#
#       so,
#           dC/dw_ij = y_i * sum_k(w_jk * (y_k-t_k)) .* y_j .* (1-y_j)  (3)
#                    = y_i * dC/dz_j
#                      np.dot(s_emb, np.transpose(der_bprop_1))
#
#   (4) dC/db_j can be written as
#
#           dC/db_j = dC/dz_j * dz_j/db_j
#
#       We know the value of dC/dz_k from (3a), and
#
#           dz_j/db_j = 1
#
#       so,
#           dC/db_j = dC/dz_j                                           (4)
#           grad_b_hid = der_bprop_1.sum(axis=1)
