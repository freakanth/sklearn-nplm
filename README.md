# Scikit-learn NPLM #

An implementation of the Neural Probabilistic Language Model in Python using
the scikit-learn library.


### Summary ###

* This repository contains an implementation of the Neural Probabilistic Language Model [1] in Python using [Scikit-learn](http://scikit-learn.org/stable/) [2].
* The model is trained using minibatch gradient-descent with momentum.
* The repository also contains an example dataset in the file *data.pkl.gz*.
* This is a very basic implementation of the model and I haven't implemented
  several other optimization, and I probably won't for now.
* The code first trains a model and saves it to the file *language-model.pkl.gz*.
* The distributed word representations learned by the model are then mapped to
  a 2D space using the t-SNE algorithm [3].
* The 2D coordinates thus obtained are used to generate a map of the words
  which reflects their co-occurrence statistics, as learned by the model.


### Execution ###

Just run the main.py file.
```
$ python main.py
```


### References ###

* [1] Bengio, Y., Ducharme, R., Vincent, P., Jauvin, C., "A Neural 
  Probabilistic Language Model", In: Journal of Machine Learning Research, 3, 
  pp. 1137--1155, 2003. 
* [2] Pedregosa, F., Varoquaux, G., Gramfort, A., Michel, V., Thirion, B., 
  Grisel, O., ... & Duchesnay, É. (2011). Scikit-learn: Machine learning in 
  Python. The Journal of Machine Learning Research, 12, 2825-2830.
* [3] Van der Maaten, L., & Hinton, G. (2008). Visualizing data using t-SNE.
  Journal of Machine Learning Research, 9(2579-2605), 85.


### Acknowledgements ###

* The Matlab code for this model was provided in the Coursera course on [Neural 
  Networks for Machine Learning](https://www.coursera.org/course/neuralnets) 
  offered by UToronto. 
* The code for rendering the words, and dimensionality reduction using t-SNE
  was obtained from Joseph Turian's repo (https://github.com/turian/textSNE).


### Contact ###

If you find any bugs, please submit an issue or email me.

* Srikanth Cherla (abfb145_at_city_dot_ac_dot_uk)
